import tkinter as tk # Python GUI module

# style information
dark_mode_background : str = "#121212"
dark_mode_foreground : str = "#1F1B24"

class Budget_Audit_Screen:

    def __init__(self):
        next_window = "empty"

        # Window and Frames
        self.__root: tk = tk.Tk()
        self.__root.title("Advanced Calculator")

        self.__root.protocol("WM_DELETE_WINDOW", self.__on_closing)

        self.__main_frame : tk.Frame = tk.Frame(self.__root, bg = dark_mode_background)

        # Top Frame
        self.__top_frame: tk.Frame = tk.Frame(self.__main_frame, bg = dark_mode_foreground)
        self.__top_middle_frame : tk.Frame = tk.Frame(self.__main_frame, bg = dark_mode_foreground)
        self.__bottome_middle_frame : tk.Frame = tk.Frame(self.__main_frame, bg = dark_mode_foreground)
        self.__bottom_frame: tk.Frame = tk.Frame(self.__main_frame, bg = dark_mode_foreground)

        # Top Middle Frame
        # todo: implement change screen and clear buttons

        self.__title_label: tk.Label = tk.Label(self.__top_frame, text = "Budget Audit", font = ("Helvetica", 16), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)
        self.__author_label: tk.Label = tk.Label(self.__top_frame, text = "By Richard Romick", font = ("Helvetica", 16), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)
        self.__title_label.pack(pady = 20)
        self.__author_label.pack(pady = 20)

        self.__change_window_button: tk.Button = tk.Button(self.__top_middle_frame, text = "Income Calculator", borderwidth = 0, bg = dark_mode_foreground, fg = "white", command = self.__switch_to_income_calculator)
        self.__clear_button: tk.Button = tk.Button(self.__top_middle_frame, text = "Clear Values", borderwidth = 0, bg = dark_mode_foreground, fg = "white", command = self.__clear_values)
        self.__change_window_button.grid(column = 0, row = 0, padx = 10, pady = 10)
        self.__clear_button.grid(column = 1, row = 0, padx = 10, pady = 10)

        # Bottom Middle Frame
        self.__bank_money_label: tk.Label = tk.Label(self.__bottome_middle_frame, text = "Money in Bank", font = ("Helvetica", 10), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)
        self.__remaining_amount_label: tk.Label = tk.Label(self.__bottome_middle_frame, text = "Remaining in Budget", font = ("Helvetica", 10), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)
        self.__unallocated_amount_label: tk.Label = tk.Label(self.__bottome_middle_frame, text = "Unnallocated in Budget", font = ("Helvetica", 10), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)
        self.__income_not_received_label: tk.Label = tk.Label(self.__bottome_middle_frame, text = "Income Not Received", font = ("Helvetica", 10), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)

        self.__bank_money_entry: tk.Entry = tk.Entry(self.__bottome_middle_frame)
        self.__remaining_amount_entry: tk.Entry = tk.Entry(self.__bottome_middle_frame)
        self.__unnallocated_amount_entry: tk.Entry = tk.Entry(self.__bottome_middle_frame)
        self.__income_not_received_entry: tk.Entry = tk.Entry(self.__bottome_middle_frame)

        self.__bank_money_label.grid(column = 0, row = 0, pady = 16)
        self.__remaining_amount_label.grid(column = 0, row = 1, pady = 8)
        self.__unallocated_amount_label.grid(column = 0, row = 2, pady = 8)
        self.__income_not_received_label.grid(column = 0, row = 3, pady = 8)

        self.__bank_money_entry.grid(column = 1, row = 0)
        self.__remaining_amount_entry.grid(column = 1, row = 1)
        self.__unnallocated_amount_entry.grid(column = 1, row = 2)
        self.__income_not_received_entry.grid(column = 1, row = 3)

        # Bottom frame
        self.__calculate_button: tk.Button = tk.Button(self.__bottom_frame, text = "Calculate Total", borderwidth = 0, bg = dark_mode_foreground, fg = "white", command = self.__calculate_total)
        self.__total_label: tk.Label = tk.Label(self.__bottom_frame, text = "Total: ", font = ("Helvetica", 10), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)
        self.__total_amount_label: tk.Label = tk.Label(self.__bottom_frame, text = "$", font = ("Helvetica", 10), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)

        self.__calculate_button.grid(column = 0, row = 0, columnspan = 2, pady = 8)
        self.__total_label.grid(column = 0, row = 1, pady = 8)
        self.__total_amount_label.grid(column = 1, row = 1, pady = 8)

        # Packing
        self.__top_frame.pack(padx = 20, pady = 20)
        self.__top_middle_frame.pack(padx = 20, pady = 20)
        self.__bottome_middle_frame.pack(padx = 20, pady = 20)
        self.__bottom_frame.pack(padx = 20, pady = 20)
        self.__main_frame.pack()

        # todo: can we get the window in the center of the screen

    def self_destruct(self):
        self.__root.destroy()

    def __switch_to_income_calculator(self):
        self.next_window = "income calculator"
        self.__root.quit()

    def __on_closing(self):
        self.next_window = "quit"
        self.__root.quit()

    def start_UI(self):
        self.__root.mainloop()

    def popup_error(self):  # pop-up window when the user enters bad data
        """Generate a pop-up window for special messages."""
        root : tk.Tk = tk.Tk()
        root.title("Error")

        main_frame: tk.Frame = tk.Frame(root, bg = dark_mode_background)
        secondary_frame: tk.Frame = tk.Frame(main_frame, bg = dark_mode_foreground)

        #root.geometry('%dx%d+%d+%d' % (w, h, x, y))
        w : tk.Label = tk.Label(secondary_frame, text = "Please enter dollar values in all fields", font = ("Helvetica", 16), bg = dark_mode_foreground, fg = "white")
        w.pack(padx = 10, pady = 10)
        b : tk.Button = tk.Button(secondary_frame, text = "OK", command = root.destroy, font = ("Helvetica", 16), bg = dark_mode_foreground, fg = "white")
        b.pack(padx = 10, pady = 10)

        secondary_frame.pack(padx = 25, pady = 25)
        main_frame.pack()
        root.mainloop()

    def __clear_values(self):
            self.__bank_money_entry.delete(0, 'end')
            self.__remaining_amount_entry.delete(0, 'end')
            self.__unnallocated_amount_entry.delete(0, 'end')
            self.__income_not_received_entry.delete(0, 'end')

            self.__total_amount_label.config(text = "$")

    def __calculate_total(self):
        result : float = 0.0

        try:
            # add these together
            if len(self.__bank_money_entry.get()) > 0: # these decision statements verify there is a value in the entry box
                result += float(self.__bank_money_entry.get())

            if len(self.__income_not_received_entry.get()) > 0:
                result += float(self.__income_not_received_entry.get())

            # then subtract these
            if len(self.__remaining_amount_entry.get()) > 0:
                result -= float(self.__remaining_amount_entry.get())

            if len(self.__unnallocated_amount_entry.get()) > 0:
                result -= float(self.__unnallocated_amount_entry.get())


            self.__total_amount_label['text'] = '$' + '{:,.2f}'.format(result)

        except ValueError: # when the user enters bad data
            self.popup_error()

class Income_Calculator_Screen:
    # Class defines behavior for our income calculator UI

    def __init__(self):
        self.next_window : String = "empty"

        # Window and Frames
        self.__root: tk = tk.Tk()
        self.__root.title("Advanced Calculator")
        self.__main_frame : tk.Frame = tk.Frame(self.__root, bg = dark_mode_background)

        self.__root.protocol("WM_DELETE_WINDOW", self.__on_closing)

        # Top Frame
        self.__top_frame: tk.Frame = tk.Frame(self.__main_frame, bg = dark_mode_foreground)
        self.__top_middle_frame : tk.Frame = tk.Frame(self.__main_frame, bg = dark_mode_foreground)
        self.__bottome_middle_frame : tk.Frame = tk.Frame(self.__main_frame, bg = dark_mode_foreground)
        self.__bottom_frame: tk.Frame = tk.Frame(self.__main_frame, bg = dark_mode_foreground)

        # Top Middle Frame
        # todo: implement change screen and clear buttons

        self.__title_label: tk.Label = tk.Label(self.__top_frame, text = "Income Calculator", font = ("Helvetica", 16), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)
        self.__author_label: tk.Label = tk.Label(self.__top_frame, text = "By Richard Romick", font = ("Helvetica", 16), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)
        self.__title_label.pack(pady = 20)
        self.__author_label.pack(pady = 20)

        self.__change_window_button: tk.Button = tk.Button(self.__top_middle_frame, text = "Budget Audit Screen", borderwidth = 0, bg = dark_mode_foreground, fg = "white", command = self.__switch_to_budget_audit)
        self.__clear_button: tk.Button = tk.Button(self.__top_middle_frame, text = "Clear Values", borderwidth = 0, bg = dark_mode_foreground, fg = "white", command = self.__clear_values)
        self.__change_window_button.grid(column = 0, row = 0, padx = 10, pady = 10)
        self.__clear_button.grid(column = 1, row = 0, padx = 10, pady = 10)

        # Bottom Middle Frame
        self.__base_pay_label: tk.Label = tk.Label(self.__bottome_middle_frame, text = "Base Pay", font = ("Helvetica", 10), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)
        self.__BAS_label: tk.Label = tk.Label(self.__bottome_middle_frame, text = "BAS", font = ("Helvetica", 10), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)
        self.__flight_pay_label: tk.Label = tk.Label(self.__bottome_middle_frame, text = "Flight Pay", font = ("Helvetica", 10), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)

        self.__fed_taxes_label: tk.Label = tk.Label(self.__bottome_middle_frame, text = "Federal Taxes", font = ("Helvetica", 10), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)
        self.__social_sec_label: tk.Label = tk.Label(self.__bottome_middle_frame, text = "Social Security", font = ("Helvetica", 10), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)
        self.__medicare_label: tk.Label = tk.Label(self.__bottome_middle_frame, text = "Medicare", font = ("Helvetica", 10), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)
        self.__SGLI_label: tk.Label = tk.Label(self.__bottome_middle_frame, text = "SGLI", font = ("Helvetica", 10), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)
        self.__SGLI_family_label: tk.Label = tk.Label(self.__bottome_middle_frame, text = "SGLI Family", font = ("Helvetica", 10), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)

        self.__base_pay_entry: tk.Entry = tk.Entry(self.__bottome_middle_frame)
        self.__BAS_entry: tk.Entry = tk.Entry(self.__bottome_middle_frame)
        self.__flight_pay_entry: tk.Entry = tk.Entry(self.__bottome_middle_frame)

        self.__fed_taxes_entry: tk.Entry = tk.Entry(self.__bottome_middle_frame)
        self.__social_sec_entry: tk.Entry = tk.Entry(self.__bottome_middle_frame)
        self.__medicare_entry: tk.Entry = tk.Entry(self.__bottome_middle_frame)
        self.__SGLI_entry: tk.Entry = tk.Entry(self.__bottome_middle_frame)
        self.__SGLI_family_entry: tk.Entry = tk.Entry(self.__bottome_middle_frame)

        self.__base_pay_label.grid(column = 0, row = 0, pady = 8)
        self.__BAS_label.grid(column = 0, row = 1, pady = 8)
        self.__flight_pay_label.grid(column = 0, row = 2, pady = 8)
        self.__fed_taxes_label.grid(column = 0, row = 3, pady = 8)
        self.__social_sec_label.grid(column = 0, row = 4, pady = 8)
        self.__medicare_label.grid(column = 0, row = 5, pady = 8)
        self.__SGLI_label.grid(column = 0, row = 6, pady = 8)
        self.__SGLI_family_label.grid(column = 0, row = 7, pady = 8)

        self.__base_pay_entry.grid(column = 1, row = 0)
        self.__BAS_entry.grid(column = 1, row = 1)
        self.__flight_pay_entry.grid(column = 1, row = 2)
        self.__fed_taxes_entry.grid(column = 1, row = 3)
        self.__social_sec_entry.grid(column = 1, row = 4)
        self.__medicare_entry.grid(column = 1, row = 5)
        self.__SGLI_entry.grid(column = 1, row = 6)
        self.__SGLI_family_entry.grid(column = 1, row = 7)

        # Bottom frame
        self.__calculate_button: tk.Button = tk.Button(self.__bottom_frame, text = "Calculate Total", borderwidth = 0, bg = dark_mode_foreground, fg = "white", command = self.__calculate_total)
        self.__total_label: tk.Label = tk.Label(self.__bottom_frame, text = "Total: ", font = ("Helvetica", 10), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)
        self.__total_amount_label: tk.Label = tk.Label(self.__bottom_frame, text = "$", font = ("Helvetica", 10), bg = dark_mode_foreground, fg = "white", width = 20, height = 1)

        self.__calculate_button.grid(column = 0, row = 0, columnspan = 2, pady = 8)
        self.__total_label.grid(column = 0, row = 1, pady = 8)
        self.__total_amount_label.grid(column = 1, row = 1, pady = 8)

        # Packing
        self.__top_frame.pack(padx = 20, pady = 20)
        self.__top_middle_frame.pack(padx = 20, pady = 20)
        self.__bottome_middle_frame.pack(padx = 20, pady = 20)
        self.__bottom_frame.pack(padx = 20, pady = 20)
        self.__main_frame.pack()

    def self_destruct(self):
        self.__root.destroy()

    def __on_closing(self):
        self.next_window = "quit"
        self.__root.quit()

    def __switch_to_budget_audit(self):
        self.next_window = "budget audit"
        self.__root.quit()

    def popup_error(self):  # pop-up window when the user enters bad data

        """Generate a pop-up window for special messages."""
        root : tk.Tk = tk.Tk()
        root.title("Error")

        main_frame: tk.Frame = tk.Frame(root, bg = dark_mode_background)
        secondary_frame: tk.Frame = tk.Frame(main_frame, bg = dark_mode_foreground)

        #root.geometry('%dx%d+%d+%d' % (w, h, x, y))
        w : tk.Label = tk.Label(secondary_frame, text = "Please enter dollar values in all fields", font = ("Helvetica", 16), bg = dark_mode_foreground, fg = "white")
        w.pack(padx = 10, pady = 10)
        b : tk.Button = tk.Button(secondary_frame, text = "OK", command = root.destroy, font = ("Helvetica", 16), bg = dark_mode_foreground, fg = "white")
        b.pack(padx = 10, pady = 10)

        secondary_frame.pack(padx = 25, pady = 25)
        main_frame.pack()

    def start_UI(self):
        self.__root.mainloop()

    def __clear_values(self):
            self.__base_pay_entry.delete(0, 'end')
            self.__BAS_entry.delete(0, 'end')
            self.__flight_pay_entry.delete(0, 'end')
            self.__fed_taxes_entry.delete(0, 'end')
            self.__social_sec_entry.delete(0, 'end')
            self.__medicare_entry.delete(0, 'end')
            self.__SGLI_entry.delete(0, 'end')
            self.__SGLI_family_entry.delete(0, 'end')

            self.__total_amount_label.config(text = "$")

    def __calculate_total(self):
        result : float = 0.0

        try:
            # add these together
            if len(self.__base_pay_entry.get()) > 0: # these decision statements verify there is a value in the entry box
                result += float(self.__base_pay_entry.get())

            if len(self.__BAS_entry.get()) > 0:
                result += float(self.__BAS_entry.get())

            if len(self.__flight_pay_entry.get()) > 0:
                result += float(self.__flight_pay_entry.get())

            # then subtract these
            if len(self.__fed_taxes_entry.get()) > 0:
                result -= float(self.__fed_taxes_entry.get())

            if len(self.__social_sec_entry.get()) > 0:
                result -= float(self.__social_sec_entry.get())

            if len(self.__medicare_entry.get()) > 0:
                result -= float(self.__medicare_entry.get())

            if len(self.__SGLI_entry.get()) > 0:
                result -= float(self.__SGLI_entry.get())

            if len(self.__SGLI_family_entry.get()) > 0:
                result -= float(self.__SGLI_family_entry.get())

            self.__total_amount_label['text'] = '$' + '{:,.2f}'.format(result)

        except ValueError: # when the user enters bad data
            self.popup_error()


# app start
next_GUI = "income calculator"

while next_GUI != "quit":
    if next_GUI == "income calculator":
        income_calculator: Income_Calculator_Screen = Income_Calculator_Screen()
        income_calculator.start_UI()
        next_GUI = income_calculator.next_window
        income_calculator.self_destruct()
    elif next_GUI == "budget audit":
        budget_auditor : Budget_Audit_Screen = Budget_Audit_Screen()
        budget_auditor.start_UI()
        next_GUI = budget_auditor.next_window
        budget_auditor.self_destruct()
    elif next_GUI == "empty":
        print("next window: an error state has occurred")
        next_GUI == "quit"
    else:
        print("next window: an error state has occurred")
        next_GUI == "quit"